17/09/2011, 2:38


  MAINTENANCE COUNTDOWN

Maintenance countdown module provides its own maintenance page with
countdown timer, time for this timer can be set in the admin interface.
Otherwise, if you don't set time, will be shown a normal Drupal
maintenance page or from your theme.

This module based on /jQuery Countdown plugin
<http://www.littlewebthings.com/projects/countdown/>/, but script was
changed, so don't try use original.


    Usage

Did you ever wanted to show your visitors when maintenance work will be
completed and website will be online again? Now you can do it. If so,
follow these steps:

 1. Enable Maintenance Countdown module,
 2. Goto maintenance page settings (admin/settings/site-maintenance)
 3. Set options
 4. Switch website to maintenance mode
 5. Enjoy!


    Features

 1. Time for countdown (months, days, hours, minutes and/or even seconds)
 2. [optional] Message that will be shown after time is up.
 3. [optional] Reload button (or no button).
 4. [optional] Auto-reload every 15 secs, after time is up.
 5. 2 themes → light and dark
 6. Watchdog messages: a) maintenance time, b) site online
 7. Simplenews integration

 

------------------------------------------------------------------------
Создано с помощью "заметки" http://drupalblog.ru/demo/fast-notes.html

© EllECTRONC, 2011 <http://drupalblog.ru/>

